<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class AppointmentExport implements FromView, ShouldAutoSize
{
    protected $appointments;

    public function __construct($appointments)
    {
        $this->appointments = $appointments;
    }

    public function view(): View
    {
        $appointments = $this->appointments;

        return view('appointments.appointments-export', [
            'appointments' => $appointments,
        ]);
    }
}
