<?php

namespace App\Exports;

use App\Order;
use App\Product;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class OrdersExport implements FromView, ShouldAutoSize
{
    protected $orders;

    public function __construct($orders)
    {
        $this->orders = $orders;
    }

    public function view(): View
    {
        $products = Product::get();
        $orders = $this->orders;

        return view('orders.orders-export', [
            'orders' => $orders,
            'products' => $products,
        ]);
    }
}
