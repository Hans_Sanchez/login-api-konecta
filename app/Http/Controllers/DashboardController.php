<?php

namespace App\Http\Controllers;

use App\User;

class DashboardController extends Controller
{
    public function index()
    {
        $user_count = User::count();
        return view('dashboard' , compact('user_count'));
    }
}

