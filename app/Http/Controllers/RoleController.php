<?php

namespace App\Http\Controllers;

use App\Permission;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Trebol\Entrust\EntrustRole;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        return view('roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            DB::beginTransaction();
            $newRole = new Role;
            $newRole->name = $request->input('name');
            $newRole->display_name = $request->input('display_name');
            $newRole->description = $request->input('description');
            $newRole->save();

            DB::commit();
            flash()->success("El rol: <b>{$newRole->name}</b> se agregó con éxito");
        }catch(\Exception $e){
            DB::rollBack();
            flash()->error("Error: ".$e->getMessage());
            return redirect()->back()->withInput($request->all());
        }
        return redirect()->route('roles-index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        $permissions = Permission::whereHas('roles', function ($query) use ($role) {
            $query->where('role_id', $role->id);
        })->get();
        return view('roles.show', compact('role', 'permissions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        if ($role->name == "admin") {
            flash()->error("Error, este rol no se puede editar");
            return redirect()->route('roles-index');
        }else{
            $modules = Permission::select("module")
                ->groupBy('module')
                ->get();

            $permissions = Permission::get();
            return view('roles.edit', compact('role','modules','permissions'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        try{
            DB::beginTransaction();
            $role->name = $request->input('name');
            $role->display_name = $request->input('display_name');
            $role->description = $request->input('description');
            $role->update();

            $role->perms()->sync($request->input('permissions'));

            DB::commit();
            flash()->success("El rol: <b>{$role->display_name}</b> se actualizó con éxito");
        }catch(\Exception $e){
            DB::rollBack();
            flash()->error("Error: ".$e->getMessage());
            return redirect()->back()->withInput($request->all());
        }
        return redirect()->route('roles-index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        if ($role->name == "admin") {
            flash()->error("Error, este rol no se puede eliminar");
            return redirect()->route('roles-index');
        }else{
            try{
                $role->delete();
                flash()->success("El rol: <b>{$role->display_name}</b> se eliminó con éxito");
            }catch(\Exception $e){
                flash()->error("Error: ".$e->getMessage());
            }
            return redirect()->route('roles-index');
        }
    }
}
