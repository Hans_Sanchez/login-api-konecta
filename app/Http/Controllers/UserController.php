<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\Paginator;

//Paquetes
use JWTAuth;
use PHPUnit\Exception;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{

    public function index(Request $request)
    {
        Paginator::useBootstrap();
        $roles = Role::orderBy('display_name')
            ->get();

        $users = array();
        foreach ($roles as $role) {
            $users[$role->name] = User::search(trim($request->input('search-' . $role->name)))
                ->where(function ($query) {
                    if (Auth::user()->roles->first()->name != 'admin') {
                        $query->where('id', Auth::user()->id);
                    }
                })
                ->whereHas('roles', function ($query) use ($role) {
                    $query->where('id', $role->id);
                })->paginate(10);
        }

        $auth = Auth::user()->id;

        return view('users.index', compact('users', 'roles', 'auth'));
    }

    public function create(Role $role)
    {
        return view('users.create', compact('role'));
    }

    public function store(Request $request, Role $role)
    {
        $rules = [
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:8',
        ];

        $messages = [
            'email.required' => 'El email es requerido',
            'email.email' => 'El campo email no tiene un formato adecuado verifique por favor',
            'email.unique' => 'El email que intenta ingresar ya existe, verifique por favor',
            'password.required' => 'La contraseña es requerida',
            'password.confirmed' => 'Verifique por favor las contraseñas, no son iguales',
            'password.min' => 'La contraseña debe contener al menos 8 caracteres',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $validator->validate($request->all());

        try {
            DB::beginTransaction();
            $newUser = new User;
            $newUser->name = $request->input('name');
            $newUser->email = $request->input('email');
            $newUser->password = bcrypt($request->input('password'));
            $newUser->document = $request->input('document');
            $newUser->address = $request->input('address');
            $newUser->save();
            $newUser->roles()->attach($role->id);

            DB::commit();
            flash()->success("El usuario <b>{$newUser->name}</b> con rol <b>{$role->display_name}</b> se agregó con éxito");

            return redirect()->route('users.index');
        } catch (\Exception $e) {
            DB::rollBack();
            flash()->error("Error: " . $e->getMessage());
            return redirect()->back()->withInput($request->all());
        }

    }

    public function edit(User $user)
    {
        $role = $user->roles()->first();
        $roles = Role::get();

        $back = route('users.index', ['search-' . $user->roles()->first()->name]);

        return view('users.edit', compact('user', 'role', 'roles', 'back'));
    }

    public function update(Request $request, User $user)
    {
        if ($request->input('password') != null && $request->input('password_confirmation') != null) {
            $rules = [
                'email' => 'required|email|unique:users,email,' . $user->id,
                'password' => 'required|confirmed|min:8',
            ];

            $messages = [
                'email.required' => 'El email es requerido',
                'email.email' => 'El campo email no tiene un formato adecuado verifique por favor',
                'email.unique' => 'El email que intenta ingresar ya existe, verifique por favor',
                'password.confirmed' => 'Verifique por favor las contraseñas, no son iguales',
                'password.min' => 'La contraseña debe contener al menos 8 caracteres',
            ];
        } else {
            $rules = [
                'email' => 'required|email|unique:users,email,' . $user->id,
            ];

            $messages = [
                'email.required' => 'El email es requerido',
                'email.email' => 'El campo email no tiene un formato adecuado verifique por favor',
                'email.unique' => 'El email que intenta ingresar ya existe, verifique por favor',
            ];
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        $validator->validate($request->all());

        try {
            DB::beginTransaction();

            $password = $user->password;

            $user->name = $request->input('name');
            $user->email = $request->input('email');


            if ($request->input('password') == null) {
                $user->password = $password;
            } else {
                $user->password = bcrypt($request->input('password'));
            }

            $user->document = $request->input('document');
            $user->address = $request->input('address');

            $user->update();

            if ($request->input('role')) {
                $user->roles()->sync($request->input('role'));
            } else {
                $user->roles()->sync($user->roles()->first()->id);
            }

            $role = $user->roles()->first();

            DB::commit();
            flash()->success("El usuario <b>{$user->name}</b> con rol <b>{$role->display_name}</b> se actualizó con éxito");
        } catch (\Exception $e) {
            DB::rollBack();
            flash()->error("Error: " . $e->getMessage());
            return redirect()->back()->withInput($request->all());
        }
        return redirect()->route('users.index', ['search-' . $role->name]);
    }

    public function destroy(User $user)
    {
        try {

            $user->delete();
            flash()->success("El usuario <b>{$user->name}</b> se movió a la papelera con éxito");
        } catch (\Exception $e) {
            flash()->error("Error: " . $e->getMessage());
        }
        return redirect()->route('users.index');
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        return response()->json(compact('token'));
    }

    public function getAuthenticatedUser()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
        return response()->json(compact('user'));
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'document' => ['required', 'string', 'max:255'],
            'address' => ['required', 'string', 'max:255'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        try {
            $user = User::create([
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'password' => Hash::make($request->get('password')),
                'document' => str_replace('_', '', $request->get('document')), //Remplazo los caracteres diferentes al número
                'address' => $request->get('address'),
            ]);

            $user->roles()->attach(2); // Por defecto le asigno a un nuevo usuario el rol de cliente

            $token = JWTAuth::fromUser($user);

            return response()->json(compact('user', 'token'), 201);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 500);
        }

    }

    public function me(): JsonResponse
    {
        try {
            return response()->json(auth()->user());
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 500);
        }
    }

    public function logout()
    {
        try {
            auth('api')->logout();
            return response()->json(['message' => 'Successfully logged out']);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 500);
        }
    }

    protected function respondWithToken(string $token): JsonResponse
    {
        try {
            return response()->json([
                'access_token' => $token,
                'token_type' => 'bearer',
                'expires_in' => auth('api')->factory()->getTTL() * 60,
            ]);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 500);
        }
    }

}
