<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserType
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::user()->roles()->first()->id != 2) {
            return $next($request);
        } else {
            Auth::logout();
            \flash()->warning("<b>Lamentamemos informarte que no tienes acceso a este apartado, si consideras que es un error por favor contactate con servicio al cliente.</b>");
            return redirect()->route('login');
        }
    }
}
