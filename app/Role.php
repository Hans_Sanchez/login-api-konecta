<?php
/**
 * Copyright (c) 2020. All Rights Reserved
 * Trébol Colombia SAS - www.trebolcolombia.com
 * Written by Jonathan Torres <jonathan8312 at gmail.com>
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Trebol\Entrust\EntrustRole;

class Role extends EntrustRole
{
    protected $fillable = ['name','display_name','description'];

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'permission_role');
    }
}
