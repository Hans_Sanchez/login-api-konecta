<?php
/**
 * Copyright (c) 2020. All Rights Reserved
 * Trébol Colombia SAS - www.trebolcolombia.com
 * Written by Jonathan Torres <jonathan8312 at gmail.com>
 */

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Trebol\Entrust\Traits\EntrustUserTrait;
//Añadimos la clase JWTSubject
use Tymon\JWTAuth\Contracts\JWTSubject;
class User extends Authenticatable implements JWTSubject
{
    use Notifiable, EntrustUserTrait {
        EntrustUserTrait::restore as entrust_restore;
    }


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'document', 'address', 'active', 'privacy_policy'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeSearch($query, $search_term)
    {
        $query->where('name', 'like', '%' . $search_term . '%')
            ->orWhere('email', 'like', '%' . $search_term . '%')
            ->orWhere('document', 'like', '%' . $search_term . '%');
    }

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
