<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            \Illuminate\Support\Facades\DB::beginTransaction();

            //USERS
            //PERMISO CONSULTAR USUARIOS
            \App\Permission::updateOrCreate([
                'name' => 'a-users-query', //ADMINISTRACIÓN - USUARIOS - CONSULTAR
            ], [
                'display_name' => 'Consulta de usuarios',
                'description' => 'El usuario podrá consultar consultar los demas usuarios del sistema',
                'module' => 'ADMINISTRACIÓN-USUARIOS',
                'created_at' => now(),
                'updated_at' => now(),
            ]);

            //PERMISO CREAR LOS DOCUMENTOS
            \App\Permission::updateOrCreate([
                'name' => 'a-users-create', //ADMINISTRACIÓN - USUARIOS - CREAR
            ], [
                'display_name' => 'Creación de usuarios',
                'description' => 'El usuario podrá crear nuevos usuarios en el sistema',
                'module' => 'ADMINISTRACIÓN-USUARIOS',
                'created_at' => now(),
                'updated_at' => now(),
            ]);

            //PERMISO EDITAR LOS DOCUMENTOS
            \App\Permission::updateOrCreate([
                'name' => 'a-users-update', //ADMINISTRACIÓN - USUARIOS - ACTUALIZAR
            ], [
                'display_name' => 'Actualización de usuarios',
                'description' => 'El usuario podrá actualizar usuarios en el sistema',
                'module' => 'ADMINISTRACIÓN-USUARIOS',
                'created_at' => now(),
                'updated_at' => now(),
            ]);

            //PERMISO ELIMINAR LOS DOCUMENTOS
            \App\Permission::updateOrCreate([
                'name' => 'a-users-destroy', //ADMINISTRACIÓN - USUARIOS - ELIMINAR
            ], [
                'display_name' => 'Eliminación de usuarios',
                'description' => 'El usuario podrá eliminar usuarios en el sistema',
                'module' => 'ADMINISTRACIÓN-USUARIOS',
                'created_at' => now(),
                'updated_at' => now(),
            ]);

            //PERMISSIONS - ESTO SON DE UNA RUTA OCULTA
            //PERMISO CONSULTAR PERMISOS
            \App\Permission::updateOrCreate([
                'name' => 'a-permissions-query', //ADMINISTRACIÓN - PERMISOS - CONSULTAR
            ], [
                'display_name' => 'Consulta de permisos',
                'description' => 'El usuario podrá consultar los permisos del sistema',
                'module' => 'ADMINISTRACIÓN-PERMISOS',
                'created_at' => now(),
                'updated_at' => now(),
            ]);

            //PERMISO CREAR PERMISOS
            \App\Permission::updateOrCreate([
                'name' => 'a-permissions-create', //ADMINISTRACIÓN - PERMISOS - CREAR
            ], [
                'display_name' => 'Creación de permisos',
                'description' => 'El usuario podrá crear nuevos permisos en el sistema',
                'module' => 'ADMINISTRACIÓN-PERMISOS',
                'created_at' => now(),
                'updated_at' => now(),
            ]);

            //PERMISO EDITAR PERMISOS
            \App\Permission::updateOrCreate([
                'name' => 'a-permissions-update', //ADMINISTRACIÓN - PERMISOS - ACTUALIZAR
            ], [
                'display_name' => 'Actualización de permisos',
                'description' => 'El usuario podrá actualizar permisos en el sistema',
                'module' => 'ADMINISTRACIÓN-PERMISOS',
                'created_at' => now(),
                'updated_at' => now(),
            ]);

            //PERMISO ELIMINAR PERMISOS
            \App\Permission::updateOrCreate([
                'name' => 'a-permissions-destroy', //ADMINISTRACIÓN - PERMISOS - ELIMINAR
            ], [
                'display_name' => 'Eliminación de permisos',
                'description' => 'El usuario podrá eliminar permisos en el sistema',
                'module' => 'ADMINISTRACIÓN-PERMISOS',
                'created_at' => now(),
                'updated_at' => now(),
            ]);

            //ROLES
            //PERMISO CONSULTAR ROLES
            \App\Permission::updateOrCreate([
                'name' => 'a-roles-query', //ADMINISTRACIÓN - ROLES - CONSULTAR
            ], [
                'display_name' => 'Consulta de roles',
                'description' => 'El usuario podrá consultar los roles del sistema',
                'module' => 'ADMINISTRACIÓN-ROLES',
                'created_at' => now(),
                'updated_at' => now(),
            ]);

            //PERMISO CREAR ROLES
            \App\Permission::updateOrCreate([
                'name' => 'a-roles-create', //ADMINISTRACIÓN - ROLES - CREAR
            ], [
                'display_name' => 'Creación de roles',
                'description' => 'El usuario podrá crear nuevos roles en el sistema',
                'module' => 'ADMINISTRACIÓN-ROLES',
                'created_at' => now(),
                'updated_at' => now(),
            ]);

            //PERMISO DETALLE ROLES
            \App\Permission::updateOrCreate([
                'name' => 'a-roles-show', //ADMINISTRACIÓN - ROLES - DETALLE
            ], [
                'display_name' => 'Detalle de roles',
                'description' => 'El usuario podrá actualizar roles en el sistema',
                'module' => 'ADMINISTRACIÓN-ROLES',
                'created_at' => now(),
                'updated_at' => now(),
            ]);

            //PERMISO EDITAR ROLES
            \App\Permission::updateOrCreate([
                'name' => 'a-roles-update', //ADMINISTRACIÓN - ROLES - ACTUALIZAR
            ], [
                'display_name' => 'Actualización de roles',
                'description' => 'El usuario podrá actualizar roles en el sistema',
                'module' => 'ADMINISTRACIÓN-ROLES',
                'created_at' => now(),
                'updated_at' => now(),
            ]);

            //PERMISO ELIMINAR ROLES
            \App\Permission::updateOrCreate([
                'name' => 'a-roles-destroy', //ADMINISTRACIÓN - ROLES - ELIMINAR
            ], [
                'display_name' => 'Eliminación de roles',
                'description' => 'El usuario podrá eliminar roles en el sistema',
                'module' => 'ADMINISTRACIÓN-ROLES',
                'created_at' => now(),
                'updated_at' => now(),
            ]);

            \Illuminate\Support\Facades\DB::commit();
        } catch (\Exception $exception) {
            \Illuminate\Support\Facades\DB::rollback();
        }
    }
}
