<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;


class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('users')->delete();

        \DB::table('users')->insert(array(
            0 =>
                array(
                    'id' => 1,
                    'name' => 'Hans Yadiel Sánchez Mora',
                    'email' => 'hanssanchez427@gmail.com',
                    'email_verified_at' => NULL,
                    'password' => bcrypt('Konecta123++'),
                    'document' => '1069766258',
                    'address' => 'Fusagasugá - Colombia',
                    'created_at' => now(),
                    'updated_at' => now(),
                ),
        ));


    }
}
