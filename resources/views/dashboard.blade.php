@extends('layout.master')

@push('plugin-styles')
    {!! Html::style('/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') !!}
@endpush

@section('content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            <h4 class="mb-3 mb-md-0">Tablero de control</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-xl-12 stretch-card">
            <div class="row flex-grow">
                <div class="col-md-4 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="text-center">
                                <h4 class="text-uppercase">Usuarios registrados</h4>
                                <hr>
                                <div style="font-size: 4em">
                                    {{$user_count}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- row -->

@endsection

@push('plugin-scripts')
    {!! Html::script('/assets/plugins/chartjs/Chart.min.js') !!}
    {!! Html::script('/assets/plugins/jquery.flot/jquery.flot.js') !!}
    {!! Html::script('/assets/plugins/jquery.flot/jquery.flot.resize.js') !!}
    {!! Html::script('/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}
    {!! Html::script('/assets/plugins/apexcharts/apexcharts.min.js') !!}
    {!! Html::script('/assets/plugins/progressbar-js/progressbar.min.js') !!}
@endpush

@push('custom-scripts')
    {!! Html::script('/assets/js/dashboard.js') !!}
    {!! Html::script('/assets/js/datepicker.js') !!}
@endpush
