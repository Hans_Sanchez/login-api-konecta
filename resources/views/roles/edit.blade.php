@extends('layout.master')

@section('content')
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Tablero de control</a></li>
            <li class="breadcrumb-item" aria-current="page">Administración</li>
            <li class="breadcrumb-item" aria-current="page">Listado de roles</li>
            <li class="breadcrumb-item active" aria-current="page">Editar</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-header">
            <strong class="text-uppercase">Editar rol {{$role->display_name}}</strong>
        </div>
        <form action="{{route('roles-update', $role)}}" method="POST" autocomplete="off">
            @csrf
            @method('put')
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name"><strong>Nombre <span class="text-danger">*</span></strong></label>
                            <input id="name" class="form-control" type="text" value="{{old('name', $role->name)}}"
                                   name="name" required readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="display_name"><strong>Nombre en pantalla <span
                                        class="text-danger">*</span></strong></label>
                            <input id="display_name" class="form-control" type="text"
                                   value="{{old('display_name', $role->display_name)}}" name="display_name" required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="description"><strong>Descripción</strong></label>
                            <input id="description" class="form-control" type="text"
                                   value="{{old('description', $role->description?$role->description:'No se registró una descripción para este rol')}}"
                                   name="description" required>
                        </div>
                    </div>
                </div>
                <br>
                <strong class="text-uppercase">Permisos</strong>
                <hr>
                <div class="card-columns">
                    @foreach($modules as $module)
                        @if($module->module != "Permisos")
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">{{$module->module}}</h5>
                                    <div class="row">
                                        @foreach($permissions->where('module', $module->module) as $permission)
                                            <div class="col-md-12">
                                                <div class="form-check">
                                                    <label for="permission-{{$permission->id}}" class="form-check-label">
                                                        <input type="checkbox" name="permissions[]"
                                                               id="permission-{{$permission->id}}"
                                                               value="{{$permission->id}}"
                                                               class="form-check-input"
                                                            {{$role->perms->where('id', $permission->id)->count()?'checked':''}}>
                                                        {{$permission->display_name}}
                                                    </label>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
            <div class="card-footer">
                <a class="btn btn-primary" href="{{ route ('roles-index')}}" role="button">
                    Volver
                </a>
                <button type="submit" class="btn btn-success updated">
                    Actualizar
                </button>
            </div>
        </form>
    </div>
@endsection
