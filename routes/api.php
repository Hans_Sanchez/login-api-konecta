<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => 'api', 'prefix' => 'auth'], function ($router) {
    Route::post('login', 'UserController@authenticate');
    Route::post('register', 'UserController@register');

});

Route::group(['middleware' => 'jwt.verify' , 'prefix' => 'auth'], function () {
    Route::post('logout', 'UserController@logout');
    Route::get('me', 'UserController@me');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
